var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on('open', function() {
    console.log('Connect successfully ^_^');

    var kittySchema = mongoose.Schema({
        name: String
    })

    kittySchema.methods.speak = function () {
        var greeting = this.name
            ? "Meow name is " + this.name
            : "I don't have a name";
        console.log(greeting);
    }

    var Kitten = mongoose.model('Kitten', kittySchema);

    var fluffy = new Kitten({ name: 'Doraemon' });
    fluffy.save(function(err, result) {
        if (err) return console.log(err);
        result.speak();
    })

    Kitten.find({name: 'Doraemon'}, (err, result) => {
        if (err) return console.log(err); 
        console.log(result);
    })
})